#!/usr/bin/env python
import argparse
import requests
import time
from multiprocessing import Process


def do_post(endpoint, query):
    """
    execute a post request.
    :param endpoint: HTTP/s API endpoint
    :param query: query to be executed
    """

    params = {'query': query }
    response = requests.post(endpoint, data=params)

    #TODO evaluate the response, as is an example i do nothing
    if response.content:
        print (response.content)


def query_generator(query_count):
    """
    Generate a number of HTTP postrequest against and API
    :param query_count: number of post execution per second
    """

    endpoint = 'http://mockbin.com/request'
    query = 'SELECT COUNT(*) FROM foo_table;'

    proc = []
    for i in range(query_count):
        p = Process(target=do_post(endpoint, query))
        p.start()
        proc.append(p)

    for p in proc:
        p.join()

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Query per second generator')

    parser.add_argument('-c',
                        action='store',
                        type=int,
                        dest='count_query_per_sec',
                        required=True,
                        help='Number of query execution/s')

    args = parser.parse_args()

    # i don't want to hurt your computer
    # query generator just run for 3 seconds

    for x in range(3):
        query_generator(args.count_query_per_sec)
        time.sleep(1)
